#
# This policy configuration will be used by all qcom products
# that inherit from AOSkjP
#

BOARD_PLAT_PRIVATE_SEPOLICY_DIR += \
    device/aoskjp/sepolicy/qcom/private

BOARD_SEPOLICY_DIRS += \
    device/aoskjp/sepolicy/qcom/common \
    device/aoskjp/sepolicy/qcom/$(TARGET_BOARD_PLATFORM)
