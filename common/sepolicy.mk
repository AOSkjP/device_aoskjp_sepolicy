#
# This policy configuration will be used by all products that
# inherit from AOSkjP
#

BOARD_PLAT_PUBLIC_SEPOLICY_DIR += \
    device/aoskjp/sepolicy/common/public

BOARD_PLAT_PRIVATE_SEPOLICY_DIR += \
    device/aoskjp/sepolicy/common/private

BOARD_SEPOLICY_DIRS += \
    device/aoskjp/sepolicy/common/vendor
